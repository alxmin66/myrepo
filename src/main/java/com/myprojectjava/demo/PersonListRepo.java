package com.myprojectjava.demo;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PersonListRepo extends CrudRepository<PersonList,Long> {
    List<PersonList> findByLastName(String lastName);
}
