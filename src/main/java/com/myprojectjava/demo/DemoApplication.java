package com.myprojectjava.demo;

import javafx.application.Application;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@SpringBootApplication
@Controller
@Configuration
@EnableAutoConfiguration


public class DemoApplication {

	@RequestMapping("/")
	@ResponseBody
	public String home() {
		return "Hello World!";
	}


	@RequestMapping("/request1")
	@ResponseBody
	public String handler(@RequestParam(name = "name") String name) {
		return "Mytest parametr  " + name;
	}

	@RequestMapping("/mytest")
	@ResponseBody
	//http://localhost:8080/mytest?param=ddd
	public String testparam(@RequestParam(name = "param") String param) {
		return "Mytest param  " + param;
	}

	@RequestMapping("/request2")
	@ResponseBody
	//http://localhost:8080/request2?firstName=dfcz&lastName=Pupkin



	public static void main(String[] args) {

		SpringApplication.run(DemoApplication.class, args);

	/*	ConfigurableApplicationContext context = SpringApplication.run(Application.class);
		PersonListRepo repository = context.getBean(PersonListRepo.class);

		// save a couple of customers
		repository.save(new PersonList("Jack", "Bauer"));
		repository.save(new PersonList("Chloe", "O'Brian"));
		repository.save(new PersonList("Kim", "Bauer"));
		repository.save(new PersonList("David", "Palmer"));
		repository.save(new PersonList("Michelle", "Dessler"));

		// fetch all customers
		Iterable<PersonList> customers = repository.findAll();
		System.out.println("Customers found with findAll():");
		System.out.println("-------------------------------");
		for (PersonList customer : customers) {
			System.out.println(customer);
		}
		System.out.println();

		// fetch an individual customer by ID
		PersonList customer = repository.findOne(1L);
		System.out.println("Customer found with findOne(1L):");
		System.out.println("--------------------------------");
		System.out.println(customer);
		System.out.println();

		// fetch customers by last name
		List<PersonList> bauers = repository.findByLastName("Bauer");
		System.out.println("Customer found with findByLastName('Bauer'):");
		System.out.println("--------------------------------------------");
		for (PersonList bauer : bauers) {
			System.out.println(bauer);
		}

		context.close();
*/
	}
}
