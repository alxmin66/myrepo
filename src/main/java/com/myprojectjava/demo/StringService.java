package com.myprojectjava.demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StringService {

    @Autowired
    SumService sumService;

    public String myString(int i, int j) {
       return  String.valueOf(sumService.sum(i,j));
    }
}
